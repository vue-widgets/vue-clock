# vue-clock

##### I found this beautiful watch
![alt text](https://gitlab.com/vue-widgets/vue-clock/-/raw/main/src/assets/concept.jpg?ref_type=heads)
##### from this page [Alarm-Clock-concept](https://dribbble.com/shots/2004657-Alarm-Clock-concept) and decided to transcribe them into vue

<br/>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).