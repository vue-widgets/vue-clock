import { createApp } from 'vue'
import App from './App.vue'
import moment from 'moment'

// createApp(App).use(router).use(store).mount('#app')
const app = createApp(App)

/**
 * @params {date} date to be converted to timeago
 * @returns returns timeAgo
 */
app.config.globalProperties.$filters = {
  timeAgo(date) {
    return moment(date).fromNow()
  },
}


app.mount('#app')
